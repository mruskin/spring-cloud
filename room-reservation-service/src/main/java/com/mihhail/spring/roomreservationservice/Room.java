package com.mihhail.spring.roomreservationservice;

public class Room {

    private long id;
    private String name;
    private String roomNumber;
    private String bedInfo;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(final String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getBedInfo() {
        return bedInfo;
    }

    public void setBedInfo(final String bedInfo) {
        this.bedInfo = bedInfo;
    }
}
