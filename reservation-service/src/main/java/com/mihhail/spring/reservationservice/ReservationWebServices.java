package com.mihhail.spring.reservationservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/reservations")
public class ReservationWebServices {

    private final ReservationRepository repository;

    public ReservationWebServices(ReservationRepository repository) {
        super();
        this.repository = repository;
    }

    @GetMapping
    Iterable<Reservation> getAllReservations(@RequestParam(name="date", required=false) Date date) {
        if (null != date) {
            return this.repository.findAllByDate(date);
        } else {
            return this.repository.findAll();
        }
    }

    @GetMapping("/{id}")
    public Reservation getReservation(@PathVariable("id") long id) {
        return this.repository.findById(id).get();
    }
}
